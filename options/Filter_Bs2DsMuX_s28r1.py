#!/usr/bin/env gaudirun.py
# 
# Stefano Cali (Many thanks to Melody Ravonel Salzgeber) 
# Filter for the stripping line:
# -b2DsMuXB2DMuForTauMuLine
# PID and HLT2 requirements removed
# 2016
#

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.Configuration import StrippingConf

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from StrippingSettings.Stripping28r1.LineConfigDictionaries_Semileptonic import B2DMuForTauMu
from StrippingSelections.StrippingSL import StrippingB2DMuForTauMu
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
# from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdLooseElectrons
from StandardParticles import StdNoPIDsPions, StdNoPIDsKaons,StdNoPIDsProtons,StdNoPIDsMuons
from GaudiKernel.SystemOfUnits import MeV, GeV, cm, mm

# Substitute the tauonic line PID cuts.
B2DMuForTauMu['CONFIG']['PIDmu']         = -99999
B2DMuForTauMu['CONFIG']['KaonPIDK']      = -99999
B2DMuForTauMu['CONFIG']['PionPIDKTight'] =  99999
B2DMuForTauMu['CONFIG']['Hlt2Line']      = "HLT_PASS_RE('Hlt2.*Decision')"
B2DMuForTauMu['CONFIG']['Hlt2LineFake']  = "HLT_PASS_RE('Hlt2.*Decision')"

MyStream = StrippingStream("B2DMuNuXANDTAU")
#MyStream = StrippingStream("Semileptonic")

confB2DMuForTauMu = StrippingB2DMuForTauMu.B2DMuForTauMuconf("B2DMuForTauMu", B2DMuForTauMu['CONFIG'])
lineList= confB2DMuForTauMu.lines()
replaceDictFilterDesktop = { 'MuforB2DMuForTauMu'  : 'Phys/StdAllNoPIDsMuons/Particles'
                             ,'PiforB2DMuForTauMu' : 'Phys/StdAllNoPIDsPions/Particles'
                             ,'KforB2DMuForTauMu'  : 'Phys/StdAllNoPIDsKaons/Particles' }
replaceDictVoidFilter = { 'SelFilterPhys_StdAllLooseMuons_Particles' : "\n            0<CONTAINS('Phys/StdAllNoPIDsMuons/Particles',True)\n            "
                          ,'SelFilterPhys_StdLooseKaons_Particles'   : "\n            0<CONTAINS('Phys/StdAllNoPIDsKaons/Particles',True)\n            "
                          ,'SelFilterPhys_StdLoosePions_Particles'   : "\n            0<CONTAINS('Phys/StdAllNoPIDsPions/Particles',True)\n            " }
for l in lineList:
    MyLines = []
    # Get the line and change in place the inputs/code in the sequences.
    if 'b2DsMuXB2DMuForTauMuLine' in l.name():
        for i in range(len(l._members)):
            seq = l._members[i]
            # Replace the inputs in the FilterDesktop sequences.
            if seq.name() in replaceDictFilterDesktop.keys():
                seq.Inputs = [ replaceDictFilterDesktop[seq.name()] ]
                l._members = l._members[:i] + [seq] + l._members[i+1:]
            # Hack the Code attribute in the void filters.
            if seq.name() in replaceDictVoidFilter.keys():
                seq.Code = replaceDictVoidFilter[seq.name()]
                l._members = l._members[:i] + [seq] + l._members[i+1:]
        MyLines.append(l)
    MyStream.appendLines( MyLines )
    
locations = []
for lin in MyStream.lines:
    print lin.outputLocation()
    locations.append(lin.outputLocation())

dstStreams  = [ "Semileptonic"]
stripTESPrefix = 'Strip'
from Configurables import ProcStatusCheck
sc = StrippingConf( Streams = [MyStream] ,
                    MaxCandidates = 20000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    Verbose = True,
                    DSTStreams = dstStreams)

# So that we do not get all events written out
MyStream.sequence().IgnoreFilterPassed = False

# Configure the dst writers for the output
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }
SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          OutputFileSuffix ='noPIDHackNoCuts',
                          SelectionSequences = sc.activeStreams() )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)
##0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number


##################################################################################################
#Stripping reportes provide the following information:
#Selection results in each event
#Summary of selection results (number of selected events, accept rate and average multiplicity) every N events.
#Summary of selection results in the end of the job.
#List of noisy selections (with the accept rate above certain threshold) and non-responding selections (that select zero events)
from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections(),ReportFrequency = 5000)
# ChronoAuditor is used by StrippingReport to show the timing.
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )
##################################################################################################


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

DaVinci().appendToMainSequence( [ eventNodeKiller,sc.sequence(),stck,dstWriter.sequence() ] )
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000


DaVinci().ProductionType = "Stripping"
DaVinci().DataType  = "2016"
DaVinci().InputType = "DST"

DaVinci().Simulation = True
DaVinci().Lumi =  not DaVinci().Simulation


# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
