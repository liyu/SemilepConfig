from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc


from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci
name = "dsttaunu"

_stdPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
_pionFilter = FilterDesktop('pionFilter',
                    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                    Code = "(P>2.0*GeV) & (PT > 300 *MeV) & (mcMatch('[pi+]cc'))"\
                   "&(TRGHOSTPROB < 0.5) & (MIPCHI2DV(PRIMARY)>9)")
PionFilterSel = Selection(name = 'PionFilterSel',
                          Algorithm = _pionFilter,
                          RequiredSelections = [ _stdPions ])


_stdKaons = DataOnDemand(Location = "Phys/StdNoPIDsKaons/Particles")                          
_kaonFilter = FilterDesktop('kaonFilter',
                    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                  Code = "(P>2.0*GeV) & (PT > 300 *MeV)"\
                   " & (mcMatch('[K+]cc'))  & (TRGHOSTPROB < 0.5)& (MIPCHI2DV(PRIMARY)>9)")
KaonFilterSel = Selection(name = 'KaonFilterSel',
                          Algorithm = _kaonFilter,
                          RequiredSelections = [ _stdKaons ])


_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
                    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                    Code = "(mcMatch('[mu+]cc')) & (TRGHOSTPROB < 0.5) & (P> 3.0*GeV) & (MIPCHI2DV(PRIMARY)>16)")
MuonFilterSel = Selection(name = 'MuonFilterSel',
                          Algorithm = _muonFilter,
                          RequiredSelections = [ _stdMuons ])
                          
                          
_makeD = CombineParticles("makeD_" + name,
                DecayDescriptor = "[D+ -> K- pi+ pi+]cc",
                CombinationCut = "(ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) > 2500 *MeV) & (ADAMASS('D+') < 100 *MeV)",
			          MotherCut = "(SUMTREE( PT,  ISBASIC )> 2500 * MeV) & (VFASPF(VCHI2/VDOF) < 4) " \
                            "& (BPVVDCHI2 > 25) &  (BPVDIRA> 0.999)"
)
selD = Selection ("SelLc",
                     Algorithm = _makeD,
                     RequiredSelections = [KaonFilterSel,PionFilterSel])
                               
_makeB = CombineParticles("SelD",
                            DecayDescriptor = "[B0 ->  D+ mu-]cc",
                            MotherCut = "(VFASPF(VCHI2/VDOF)< 6) & (BPVDIRA> 0.999)"
                            )
selB = Selection ("Sel"+name,
                     Algorithm = _makeB,
                     RequiredSelections = [selD,MuonFilterSel])


seq = SelectionSequence("Dplustaunu.SafeStripTrig", 
                          TopSelection = selB)

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [seq]
                          )
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )


