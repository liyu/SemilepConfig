"""
Options for building Stripping17 with strict ordering
of streams such that the micro-DSTs come last.

Options file for MC, with stripping filter for b2D0MuX semileptonic stream only.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []


#streams.append( quickBuild('Semileptonic') )

semi=quickBuild('Semileptonic')
#semi.lines = []
semi.lines[:] = [ x for x in semi.lines if (('b2D0MuX' in x.name()) and not ('b2D0MuXK3Pi' in x.name())) ]
#semi.lines[:] = [ x for x in semi.lines if (('b2D0MuXB2DMuNuX' in x.name()) and not ('b2D0MuXK3Pi' in x.name())) ]

print "print lines"
for line in semi.lines :
    print "semi has a line called " + line.name()

    
streams.append( semi )

#
# turn off all pre-scalings 
#
for stream in streams: 
    for line in stream.lines:
        line._prescale = 1.0 


#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("b2D0MuX.Strip")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    HDRLocation="Phys/DecReports",
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get all events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

#
# Configuration of SelDSTWriter
#

SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"
   
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().UseTrigRawEvent=True

